﻿using APIDemo.db;
using APIDemo.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly AppDbContext context;

        public UsersController(AppDbContext context)
        {
            this.context = context;
        }
        [HttpGet]
        public IEnumerable<User> GetUsers()
        {

            return context.Users.ToList();
        }
        [HttpPost]
        public string AddUser(User user)
        {
            try
            {
                context.Users.Add(user);
                context.SaveChanges();
                return "User registered successfully.";
            }
            catch(Exception xe)
            {
                return "User  not registered successfully. Try again.";
            }
           
        }
        [HttpDelete("{id}")]
        public string DeleteUser(int id)
        {
            var result = context.Users.Find(id);
            if (result != null)
            {
                context.Remove(result);
                context.SaveChanges();
                return "User deleted.";
            }
            else
            {
                return "User dosen't exsit.";
            }

        }
        [HttpGet("{id}")]
        public User GetUserById(int id)
        {
            var result = context.Users.Find(id);
            if (result != null)
            {
               
                return result;
            }
            else
            {
                return null;
            }

        }
        [HttpPatch("{id}")]
        public string UpadteUser(int id,User user)
        {
            var result = context.Users.Find(id);
            if (result != null)
            {
                result.Firstname = user.Firstname;
                result.LastName = user.LastName;
                result.Email = user.Email;
                context.Update(result);
                context.SaveChanges();
                return "User updated.";
            }
            else
            {
                return "User dosen't exsit.";
            }

        }
    }
}
