﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIDemo.Models
{
    public class User
    {
        public int ID { get; set; }
        public string Firstname { get; set; }
        public string LastName  { get; set; }
        public string Email { get; set; }
    }
}
